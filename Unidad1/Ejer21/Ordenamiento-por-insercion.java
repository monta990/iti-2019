package ordenamientoporinsercion;

public class OrdenamientoporInsercion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int []arreglo = {5,11,7,4,45,67,2,4,6,7,1,3,2,5};
        Ordenador o = new Ordenador();
        o.ordenarInsercion(arreglo);
        
        for(int i=0;i<arreglo.length;i++){
            System.out.println(arreglo[i]);
        }
    }
    
}


//CLASE ORDENADOR

package ordenamientoporinsercion;

/**
 *
 * @author luis duran
 */
public class Ordenador {
    public void ordenarInsercion(int [] array){
        int aux;
        int cont1;
        int cont2;
        for(cont1 = 1; cont1<array.length; cont1++){
            
            aux=array[cont1];
            for(cont2=cont1-1;cont2 >=0 && array[cont2]>aux;cont2--)
            {
                array[cont2+1]=array[cont2];
                array[cont2]=aux;
            }
        }
    }
    
}