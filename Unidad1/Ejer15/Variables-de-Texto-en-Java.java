package variablestipotexto;
import java.io.*;

public class VariablesTipoTexto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //Escribiendo un texto
        String texto = "";
        int x = 0;
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader buffer = new BufferedReader(in);
        
        try{
            texto = buffer.readLine();
            x = Integer.parseInt(texto);
        }catch(IOException e){
        System.out.println("Debes escribir un número");
        };
        //Concatenación
        //texto = texto.concat(" Esto Es una concAtenación");
        //Pasa todo a minusculas
        //texto = texto.toLowerCase();
        
        //Pasa todo a mayusculas
        //texto = texto.toUpperCase();
       // int a = texto.length();
       // System.out.println(texto);
       
       //Impresión del texto escrito.
       // System.out.println("La frase tiene "+a+" letras");
      
       
       //Impresión de la variable
       System.out.println(x);
    }
    
}