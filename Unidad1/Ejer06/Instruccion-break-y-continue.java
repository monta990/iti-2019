package sentenciabreakcontinue;

public class SentenciaBreakContinue {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
      //CONTINUE
      for(int i=0; i<=10; i++){
          System.out.println("Aún estás en el ciclo");
          if(i==4){
              continue;
          }
          System.out.println("El valor de i es:"+i);
      }
      System.out.println("Has dejado el ciclo for");
      
      //BREAK
      System.out.println("---------------------------");
      System.out.println("EMPIEZA EL CICLO DEL BREAK");
       for(int x=0; x<=10; x++){
          System.out.println("Aún estás en el ciclo");
          if(x==4){
              break;
          }
          System.out.println("El valor de i es:"+x);
      }
      System.out.println("Has dejado el ciclo for");
    }
    
}
