package herencia;
import herencia.Carro;

public class Herencia {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Carro c;
        c = new Carro();
        c.apagar();
        c.encender();
        
        CarroBMW bmw;
        bmw = new CarroBMW();
        bmw.turbo();
        
        CarroToyota t;
        t = new CarroToyota();
        t.modelo();
        
    }
    
}
