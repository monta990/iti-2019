package polimorfismo;

import polimorfismo.Carro;

public class Polimorfismo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Carro c;
        c = new Carro();
        c.informacion();
        
        CarroBMW carrito;
        carrito = new CarroBMW();
        carrito.informacion();
    }
    
}