﻿namespace CRUD
{
	partial class UNIDAD_2
	{
		/// <summary>
		/// Variable del diseñador necesaria.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Limpiar los recursos que se estén usando.
		/// </summary>
		/// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Código generado por el Diseñador de Windows Forms

		/// <summary>
		/// Método necesario para admitir el Diseñador. No se puede modificar
		/// el contenido de este método con el editor de código.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtBox1 = new MetroFramework.Controls.MetroTextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.Grid1 = new System.Windows.Forms.DataGridView();
			this.txtBox2 = new MetroFramework.Controls.MetroTextBox();
			this.btnActualizar = new MetroFramework.Controls.MetroButton();
			this.Lb2 = new System.Windows.Forms.Label();
			this.btnEliminar = new MetroFramework.Controls.MetroButton();
			this.btnAgregar = new MetroFramework.Controls.MetroButton();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
			this.SuspendLayout();
			// 
			// txtBox1
			// 
			// 
			// 
			// 
			this.txtBox1.CustomButton.Image = null;
			this.txtBox1.CustomButton.Location = new System.Drawing.Point(127, 1);
			this.txtBox1.CustomButton.Name = "";
			this.txtBox1.CustomButton.Size = new System.Drawing.Size(21, 21);
			this.txtBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
			this.txtBox1.CustomButton.TabIndex = 1;
			this.txtBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
			this.txtBox1.CustomButton.UseSelectable = true;
			this.txtBox1.CustomButton.Visible = false;
			this.txtBox1.Lines = new string[0];
			this.txtBox1.Location = new System.Drawing.Point(58, 130);
			this.txtBox1.MaxLength = 32767;
			this.txtBox1.Name = "txtBox1";
			this.txtBox1.PasswordChar = '\0';
			this.txtBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.txtBox1.SelectedText = "";
			this.txtBox1.SelectionLength = 0;
			this.txtBox1.SelectionStart = 0;
			this.txtBox1.ShortcutsEnabled = true;
			this.txtBox1.Size = new System.Drawing.Size(149, 23);
			this.txtBox1.TabIndex = 1;
			this.txtBox1.UseSelectable = true;
			this.txtBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
			this.txtBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(55, 114);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(81, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Nuevo Registro";
			// 
			// Grid1
			// 
			this.Grid1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
			this.Grid1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
			this.Grid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.Grid1.GridColor = System.Drawing.SystemColors.ControlLightLight;
			this.Grid1.Location = new System.Drawing.Point(58, 177);
			this.Grid1.Name = "Grid1";
			this.Grid1.Size = new System.Drawing.Size(494, 150);
			this.Grid1.TabIndex = 4;
			this.Grid1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Grid1_CellClick);
			// 
			// txtBox2
			// 
			// 
			// 
			// 
			this.txtBox2.CustomButton.Image = null;
			this.txtBox2.CustomButton.Location = new System.Drawing.Point(187, 1);
			this.txtBox2.CustomButton.Name = "";
			this.txtBox2.CustomButton.Size = new System.Drawing.Size(21, 21);
			this.txtBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
			this.txtBox2.CustomButton.TabIndex = 1;
			this.txtBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
			this.txtBox2.CustomButton.UseSelectable = true;
			this.txtBox2.CustomButton.Visible = false;
			this.txtBox2.Lines = new string[0];
			this.txtBox2.Location = new System.Drawing.Point(556, 204);
			this.txtBox2.MaxLength = 32767;
			this.txtBox2.Name = "txtBox2";
			this.txtBox2.PasswordChar = '\0';
			this.txtBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
			this.txtBox2.SelectedText = "";
			this.txtBox2.SelectionLength = 0;
			this.txtBox2.SelectionStart = 0;
			this.txtBox2.ShortcutsEnabled = true;
			this.txtBox2.Size = new System.Drawing.Size(209, 23);
			this.txtBox2.TabIndex = 5;
			this.txtBox2.UseSelectable = true;
			this.txtBox2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
			this.txtBox2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
			// 
			// btnActualizar
			// 
			this.btnActualizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			this.btnActualizar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnActualizar.Location = new System.Drawing.Point(558, 251);
			this.btnActualizar.Name = "btnActualizar";
			this.btnActualizar.Size = new System.Drawing.Size(209, 35);
			this.btnActualizar.TabIndex = 6;
			this.btnActualizar.Text = "Actualizar";
			this.btnActualizar.UseSelectable = true;
			this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
			// 
			// Lb2
			// 
			this.Lb2.AutoSize = true;
			this.Lb2.Location = new System.Drawing.Point(602, 177);
			this.Lb2.Name = "Lb2";
			this.Lb2.Size = new System.Drawing.Size(114, 13);
			this.Lb2.TabIndex = 7;
			this.Lb2.Text = "Registro Seleccionado";
			// 
			// btnEliminar
			// 
			this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
			this.btnEliminar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnEliminar.Location = new System.Drawing.Point(558, 292);
			this.btnEliminar.Name = "btnEliminar";
			this.btnEliminar.Size = new System.Drawing.Size(209, 35);
			this.btnEliminar.TabIndex = 8;
			this.btnEliminar.Text = "Eliminar";
			this.btnEliminar.UseSelectable = true;
			this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
			// 
			// btnAgregar
			// 
			this.btnAgregar.BackColor = System.Drawing.Color.Lime;
			this.btnAgregar.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnAgregar.Location = new System.Drawing.Point(233, 130);
			this.btnAgregar.Name = "btnAgregar";
			this.btnAgregar.Size = new System.Drawing.Size(118, 23);
			this.btnAgregar.TabIndex = 3;
			this.btnAgregar.Text = "Agregar";
			this.btnAgregar.Theme = MetroFramework.MetroThemeStyle.Light;
			this.btnAgregar.UseSelectable = true;
			this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
			// 
			// UNIDAD_2
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(790, 376);
			this.Controls.Add(this.btnEliminar);
			this.Controls.Add(this.Lb2);
			this.Controls.Add(this.btnActualizar);
			this.Controls.Add(this.txtBox2);
			this.Controls.Add(this.Grid1);
			this.Controls.Add(this.btnAgregar);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txtBox1);
			this.Name = "UNIDAD_2";
			this.Text = "UNIDAD 2";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private MetroFramework.Controls.MetroTextBox txtBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView Grid1;
		private MetroFramework.Controls.MetroTextBox txtBox2;
		private MetroFramework.Controls.MetroButton btnActualizar;
		private System.Windows.Forms.Label Lb2;
		private MetroFramework.Controls.MetroButton btnEliminar;
		private MetroFramework.Controls.MetroButton btnAgregar;
	}
}

