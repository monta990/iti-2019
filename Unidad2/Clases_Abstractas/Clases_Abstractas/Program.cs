﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clases_Abstractas
{
	abstract class Mensaje
	{
		public abstract string saludo_personalizado(string mensaje);
	}

	class Program : Mensaje
	{
		static void Main(string[] args)
		{
			
		}
		/*
		 * 1.- clas clases abstractas no pueden ser instanciadas 
		 * 2.- las clases abstractas deben ser heredadas para poder ser implementadas en su totalidad o parcialemnte a dieferencia de una interfaz
		*/
		public override string saludo_personalizado(string mensaje)
		{
			return "hola: "+mensaje;
		}
	}
}
