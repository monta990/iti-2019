﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{

	interface Ihola
	{
		void Hola();
	}

	class Program : Ihola
	{

		static void Main(string[] args)
		{



		}
		/*
			1.- en las interfaces solamente se definen las funciones que deseamos implementar posteriormente
			2.- toda clase que implemente una interfaz debe utilizar todas funciones de lo contrario no sera posible usar dicha interfaz
			3.- se declara la funcionalidad de las funciones unicamente en la clase donde se implemente la interfaz pero no en la propia interfaz
			4.- si quitamos una de las funciones que fueron definidas en la interfaz dara error 
		*/
		public void Hola()
		{
			Console.WriteLine("funcion creada desde la interface e implementada en la clase Program");
		}
	}
}
